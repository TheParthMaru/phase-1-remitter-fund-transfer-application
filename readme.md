# Version 1.0

- Just the UI that was sent to Tamil Sir.

# Version 1.1

- Renamed pages to provide meaningful name.
- Testing the JS validation on index.html with error and success classes for each role.
- After validating the information was unable to redirect admin to admin-dashboard.html and remitter to remitter-dashboard.html
- Added comments to scripts for myself and also for people who will evaluate.

# What's in version 1.2

- Restructured project files.
- Updated navbar in index.html and remitter-register.html
- Added description on working of particular functions in script.js and also mentioned reasons to use setTimeout function.
- Added the functionality to redirect admin to admin dashboard and remitter to remitter dashboard after login.
- Removed a bug with success class whose description is mentioned is script.js file line 11.
- Added admin validation and new remitter validation.
- Setting links to proper pages.
- Removed all the unneccessary commented code.

## Added admin and remitter validations in index.html

- Role: Admin
  -- Username: admin
  -- Password: admin

- Role: Remitter
  -- Username: user
  -- Password: user

## Added validations for remitter-registration.html

- Name
  -- Cannot be blank
  -- Cannot be less than 3 characters

- Account Number
  -- Cannot be blank
  -- Cannot be less than 5 digits

- Email
  -- Cannot be blank
  -- Regex pattern

- Password
  -- Cannot be blank
  -- Should be minimum 8 characters long

- Confirm Password
  -- Cannot be blank
  -- Should match password

- Address
  -- Cannot be blank

- Account Type
  -- If account type is savings minimum account balance has to be Rs 5000.
  -- For salary account, you can enter balance a minimum balance as 0. Please do not keep blank

- Account Balance
  -- Cannot be blank

## Added Beneficiary Validation for beneficiary-registratioon.html

- Name
  -- Cannot be blank
  -- Cannot be less than 3 characters

- Account Number
  -- Cannot be blank
  -- Cannot be less than 5 digits

- Email
  -- Cannot be blank
  -- Regex pattern

- Account Type
  -- If account type is savings minimum account balance has to be Rs 5000.
  -- For salary account, you can enter balance a minimum balance as 0. Please do not keep blank

- IFSC Code
  -- Cannot be blank

- Max Account Transfer Limit
  -- Cannot be blank

## Added Fund Transfer Validations in fund-transfer.html

- Selecting Beneficiary account
  -- Need to select one account atleast

- Amount
  -- Cannot be blank
  -- Needs to be greater than 0
  -- Will add the functionality and validation of deducting amount from remitter's account and validating if sufficient amount or not after learning DB.

- Remitter's account number
  -- Cannot be empty
  -- Account number has to be of 5 digits

- Transaction Narration
  -- Cannot be empty
