const form = document.querySelector("#remitter-registration");

form.addEventListener("submit", (e) => {
  e.preventDefault();

  validate();
});

// Redirecting to remitter dashboard
const sendRemitterData = (sRate, count) => {
  if (sRate === count) {
    setTimeout(function () {
      alert("Remitter Login Successful");
      location.href = `remitter-dashboard.html`;
    }, 800);
  }
};

// Remitter Validated
const remitterValidated = () => {
  let formField = document.getElementsByClassName("form-field");
  for (var i = 0; i < formField.length; i++) {
    var count = formField.length - 1;
    if (formField[i].className === "form-field success") {
      var sRate = 0 + i;
      sendRemitterData(sRate, count);
    } else {
      return false;
    }
  }
};

// validate function definition
const validate = () => {
  const name = document.querySelector("#name");
  const accNumber = document.querySelector("#accNumber");
  const email = document.querySelector("#email");
  const password = document.querySelector("#password");
  const cpassword = document.querySelector("#cpassword");
  const address = document.querySelector("#address");
  const accBalance = document.querySelector("#accBalance");
  const select = document.querySelector("#accType");
  const accType = select.options[select.selectedIndex];

  // Triming values
  const nameValue = name.value.trim();
  const accNumberValue = accNumber.value.trim();
  const emailValue = email.value.trim();
  const passwordValue = password.value.trim();
  const password2Value = cpassword.value.trim();
  const addressValue = address.value.trim();
  const accBalanceValue = accBalance.value.trim();
  const accTypeValue = accType.value.trim();

  // console.log(nameValue);
  // console.log(accNumberValue);
  // console.log(emailValue);
  // console.log(passwordValue);
  // console.log(addressValue);
  // console.log(accBalanceValue);
  // console.log(accTypeValue);

  // Validating the default behaviour
  if (
    nameValue === "" &&
    accNumberValue === "" &&
    emailValue === "" &&
    passwordValue === "" &&
    password2Value === "" &&
    addressValue === "" &&
    accBalanceValue === "" &&
    accTypeValue === "no-value"
  ) {
    alert("Please fill the form");
  }

  // Validating name
  if (nameValue === "") {
    setErrorMsg(name, "Name cannot be blank");
  } else if (nameValue.length < 3) {
    setErrorMsg(name, "Name cannot be less than 3 characters");
  } else {
    setSuccessMsg(name);
  }

  // Validating account number
  if (accNumberValue === "") {
    setErrorMsg(accNumber, "Account Number cannot be blank");
  } else if (accNumberValue.length != 5) {
    setErrorMsg(accNumber, "Invalid Account Number. Needs to be of 5 digits");
  } else {
    setSuccessMsg(accNumber);
  }

  // Validating email
  if (emailValue === "") {
    setErrorMsg(email, "Email cannot be blank");
  } else if (!isEmail(emailValue)) {
    setErrorMsg(email, "Email is not valid");
  } else {
    setSuccessMsg(email);
  }

  // Validating Password
  if (passwordValue === "") {
    setErrorMsg(password, "Password cannot be blank");
  } else if (passwordValue.length < 8) {
    setErrorMsg(password, "Password should be minimum 8 characters long");
  } else {
    setSuccessMsg(password);
  }

  // Confirming Passwords
  if (password2Value === "") {
    setErrorMsg(cpassword, "Password cannot be blank");
  } else if (passwordValue !== password2Value) {
    setErrorMsg(cpassword, "Passwords do not match");
  } else {
    setSuccessMsg(cpassword);
  }

  // Validating address
  if (addressValue === "") {
    setErrorMsg(address, "Address cannot be blank");
  } else {
    setSuccessMsg(address);
  }

  // Default account balance validation
  if (accBalanceValue === "") {
    setErrorMsg(accBalance, "Account Balance cannot be blank");
  }

  // Validating Account Type and Account Balance
  if (accTypeValue === "no-value") {
    alert("Please select account type");
  } else if (accTypeValue === "savings" && accBalanceValue < 5000) {
    setErrorMsg(
      accBalance,
      "Minimum ₹5000 required to be maintained in Savings type account"
    );
  } else if (accTypeValue === "salary" && accBalanceValue === "") {
    setErrorMsg(
      accBalance,
      "For salary account, you can enter a minimum balance as 0. Please do not keep blank"
    );
  } else {
    setSuccessMsg(accBalance);
  }

  remitterValidated();
};

// Function using regex to validate email
function isEmail(emailValue) {
  return /^[a-zA-Z0-9]{3,30}[@][a-zA-Z]{3,7}[.][a-zA-Z]{2,3}$/.test(emailValue);
}

// Function for displaying error msgs
function setErrorMsg(input, message) {
  const formField = input.parentElement;
  const small = formField.querySelector("small");
  formField.className = "form-field error";
  small.innerText = message;
}

// Function for displaying success msgs
function setSuccessMsg(input) {
  const formField = input.parentElement;
  formField.className = "form-field success";
}
