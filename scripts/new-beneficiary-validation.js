const form = document.querySelector("#beneficiary-registration");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  validate();
});

// Redirecting to remitter dashboard
const sendBeneficiaryData = (sRate, count) => {
  if (sRate === count) {
    setTimeout(function () {
      alert("Beneficiary Successfully Added");
      location.href = `remitter-dashboard.html`;
    }, 800);
  }
};

// Beneficiary Validated
const beneficiaryValidated = () => {
  let formField = document.getElementsByClassName("form-field");
  for (var i = 0; i < formField.length; i++) {
    var count = formField.length - 1;
    if (formField[i].className === "form-field success") {
      var sRate = 0 + i;
      sendBeneficiaryData(sRate, count);
    } else {
      return false;
    }
  }
};

// validate function definition
const validate = () => {
  const name = document.querySelector("#name");
  const email = document.querySelector("#email");
  const accNumber = document.querySelector("#accNumber");
  const selectAccountType = document.querySelector("#accType");
  const accType = selectAccountType.options[selectAccountType.selectedIndex];
  const selectAccountStatus = document.querySelector("#accStatus");
  const accStatus =
    selectAccountStatus.options[selectAccountStatus.selectedIndex];
  const ifsc = document.querySelector("#ifsc");
  const maxLimit = document.querySelector("#maxLimit");

  // Trimming values
  const nameValue = name.value.trim();
  const emailValue = email.value.trim();
  const accNumberValue = accNumber.value.trim();
  const accTypeValue = accType.value.trim();
  const accStatusValue = accStatus.value.trim();
  const ifscCode = ifsc.value.trim();
  const maxLimitValue = maxLimit.value.trim();

  // checking whether taking inputs or not.
  console.log(nameValue);
  console.log(emailValue);
  console.log(accNumberValue);
  console.log(accTypeValue);
  console.log(accStatusValue);
  console.log(ifscCode);
  console.log(maxLimitValue);

  // Validating the default behaviour
  if (
    nameValue === "" &&
    emailValue === "" &&
    accNumberValue === "" &&
    accTypeValue === "no-value" &&
    accStatusValue === "no-value" &&
    ifscCode === "" &&
    maxLimitValue === ""
  ) {
    alert("Please fill the form");
  }

  // Validating name
  if (nameValue === "") {
    setErrorMsg(name, "Name cannot be blank");
  } else if (nameValue.length < 3) {
    setErrorMsg(name, "Name cannot be less than 3 characters");
  } else {
    setSuccessMsg(name);
  }

  // Validating email
  if (emailValue === "") {
    setErrorMsg(email, "Email cannot be blank");
  } else if (!isEmail(emailValue)) {
    setErrorMsg(email, "Email is not valid");
  } else {
    setSuccessMsg(email);
  }

  // Validating account number
  if (accNumberValue === "") {
    setErrorMsg(accNumber, "Account Number cannot be blank");
  } else if (accNumberValue.length != 5) {
    setErrorMsg(accNumber, "Invalid Account Number. Needs to be of 5 digits");
  } else {
    setSuccessMsg(accNumber);
  }

  // Validating Account Type
  if (accTypeValue === "no-value") {
    alert("Please select account type");
  }

  // Validating Account Status
  if (accTypeValue === "no-value") {
    alert("Please select account status");
  }

  // Validating IFSC Code
  if (ifscCode === "") {
    setErrorMsg(ifsc, "IFSC code cannot be blank");
  } else {
    setSuccessMsg(ifsc);
  }

  // Validating maximum account transfer limit
  if (maxLimitValue === "") {
    setErrorMsg(maxLimit, "Please enter max amount transfer limit");
  } else {
    setSuccessMsg(maxLimit);
  }

  beneficiaryValidated();
};

// Function using regex to validate email
function isEmail(emailValue) {
  return /^[a-zA-Z0-9]{3,30}[@][a-zA-Z]{3,7}[.][a-zA-Z]{2,3}$/.test(emailValue);
}

// Function for displaying error msgs
function setErrorMsg(input, message) {
  const formField = input.parentElement;
  const small = formField.querySelector("small");
  formField.className = "form-field error";
  small.innerText = message;
}

// Function for displaying success msgs
function setSuccessMsg(input) {
  const formField = input.parentElement;
  formField.className = "form-field success";
}
