// Validation for admin and remitter login
const form = document.querySelector("#login-form");

// Adding event to prevent the default submission of form
form.addEventListener("submit", (e) => {
  e.preventDefault();
  validate();
});

// Redirecting to admin homepage
// Added timeout because it was redirecting without providing the success class to password. So to display the success class added setTimeout. Success class was working but the response was quite fast so unable to see it.

// only moving to the next page when every form-field has a success class
const sendAdminData = (sRate, count) => {
  if (sRate === count) {
    setTimeout(function () {
      alert("Admin Login Successful");
      location.href = `admin-dashboard.html`;
    }, 800);
  }
};

// Admin Verified
// here sRate is success rate. So first it will check whether for every element with form-field class, success class is appended or not. Count is the count of elements with class form-field and sRate is the count of elements  with classes success
const adminVerified = () => {
  let formField = document.getElementsByClassName("form-field");
  for (var i = 0; i < formField.length; i++) {
    var count = formField.length - 1;
    if (formField[i].className === "form-field success") {
      var sRate = 0 + i;
      sendAdminData(sRate, count);
    } else {
      return false;
    }
  }
};

// Redirecting to remitter dashboard
// Added timeout because it was redirecting without providing the success class to password. So to display the success class added setTimeout. Success class was working but the response was quite fast so unable to see it.

const sendRemitterData = (sRate, count) => {
  if (sRate === count) {
    setTimeout(function () {
      alert("Remitter Login Successful");
      location.href = `remitter-dashboard.html`;
    }, 800);
  }
};

// Remitter Verified
const remitterVerified = () => {
  let formField = document.getElementsByClassName("form-field");
  for (var i = 0; i < formField.length; i++) {
    var count = formField.length - 1;
    if (formField[i].className === "form-field success") {
      var sRate = 0 + i;
      sendRemitterData(sRate, count);
    } else {
      return false;
    }
  }
};

// Defining validate function
// used lusername and lpassword for login page as it is conflicting with other forms.
const validate = () => {
  // taking inputs value using dom
  const username = document.querySelector("#lusername");
  const password = document.querySelector("#lpassword");
  const select = document.querySelector("#role");
  const value = select.options[select.selectedIndex];

  // trimming the inputs to remove whitespaces
  const usernameValue = username.value.trim();
  const passwordValue = password.value.trim();
  const role = value.value.trim();

  // Debugging purpose
  // console.log(usernameValue);
  // console.log(passwordValue);
  // console.log(role);

  // Validating default behaviour
  if (role === "no-value") {
    alert("Please select role");
  }

  // Validating admin credentials
  // Username: admin
  // Password: admin
  if (role === "admin") {
    // validating admin username
    if (usernameValue === "") {
      setErrorMsg(username, "Username cannot be blank");
    } else if (usernameValue !== "admin") {
      setErrorMsg(username, "Invalid Username");
    } else {
      setSuccessMsg(username);
    }

    // validating admin password
    if (passwordValue === "") {
      setErrorMsg(password, "Password cannot be blank");
    } else if (passwordValue !== "admin") {
      setErrorMsg(password, "Invalid Password");
    } else {
      setSuccessMsg(password);
    }

    adminVerified();
  }

  // Validating remitter credentials
  // username: user
  // password: user
  if (role === "remitter") {
    // validating remitter username
    if (usernameValue === "") {
      setErrorMsg(username, "Username cannot be blank");
    } else if (usernameValue !== "user") {
      setErrorMsg(username, "Invalid Username");
    } else {
      setSuccessMsg(username);
    }

    // validating remitter password
    if (passwordValue === "") {
      setErrorMsg(password, "Password cannot be blank");
    } else if (passwordValue !== "user") {
      setErrorMsg(password, "Invalid Password");
    } else {
      setSuccessMsg(password);
    }
    remitterVerified();
  }
};

// Function for displaying error msgs
function setErrorMsg(input, message) {
  const formField = input.parentElement;
  const small = formField.querySelector("small");
  formField.className = "form-field error";
  small.innerText = message;
}

// Function for displaying success msgs
function setSuccessMsg(input) {
  const formField = input.parentElement;
  formField.className = "form-field success";
}
