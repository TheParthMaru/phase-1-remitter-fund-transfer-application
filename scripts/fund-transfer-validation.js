const form = document.querySelector("#fund-transfer-form");

form.addEventListener("submit", (e) => {
  e.preventDefault();
  validate();
});

// Redirecting to remitter dashboard
const sendTransferData = (sRate, count) => {
  if (sRate === count) {
    setTimeout(function () {
      alert("Fund Transfer Successful");
      location.href = `remitter-dashboard.html`;
    }, 800);
  }
};

// Successful fund transfer
const successfullyTransferred = () => {
  let formField = document.getElementsByClassName("form-field");
  for (var i = 0; i < formField.length; i++) {
    var count = formField.length - 1;
    if (formField[i].className === "form-field success") {
      var sRate = 0 + i;
      sendTransferData(sRate, count);
    } else {
      return false;
    }
  }
};

// validate function definition
const validate = () => {
  // Taking inputs from form
  const select = document.querySelector("#accNo");
  const beneficiary = select.options[select.selectedIndex];
  const amt = document.querySelector("#amount");
  const rAccNo = document.querySelector("#raccno");
  const transNarr = document.querySelector("#trans");

  // Trimming values
  const beneficiaryValue = beneficiary.value.trim();
  const amount = amt.value.trim();
  const remitterAccNo = rAccNo.value.trim();
  const transactionNarration = transNarr.value.trim();

  // Default validation
  if (
    beneficiaryValue === "no-value" &&
    amount === "" &&
    remitterAccNo === "" &&
    transactionNarration === ""
  ) {
    alert("Please fill the form");
  }

  // validating the selection of beneficiary
  if (beneficiaryValue === "no-value") {
    alert("Please select beneficiary");
  }

  // validating amount
  if (amount === "") {
    setErrorMsg(amt, "Please enter amount");
  } else if (amount < 1) {
    setErrorMsg(amt, "Please enter valid amount");
  } else {
    setSuccessMsg(amt);
  }

  // Validating account number
  if (remitterAccNo === "") {
    setErrorMsg(rAccNo, "Account Number cannot be blank");
  } else if (remitterAccNo.length != 5) {
    setErrorMsg(rAccNo, "Invalid Account Number. Needs to be of 5 digits");
  } else {
    setSuccessMsg(rAccNo);
  }

  // Validating transaction narration
  if (transactionNarration === "") {
    setErrorMsg(transNarr, "Cannot be blank");
  } else {
    setSuccessMsg(transNarr);
  }

  successfullyTransferred();
};

// Function for displaying error msgs
function setErrorMsg(input, message) {
  const formField = input.parentElement;
  const small = formField.querySelector("small");
  formField.className = "form-field error";
  small.innerText = message;
}

// Function for displaying success msgs
function setSuccessMsg(input) {
  const formField = input.parentElement;
  formField.className = "form-field success";
}
